const express = require('express');
const path = require('path')
const http = require('http');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors')
const products = require('./db');

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(3001, () => {
  console.log('Listen on 0.0.0.0:3001');
});

app.get('/food', (req, res) => {
  const page = req.query.page || 0
  const pageSize = req.query.pageSize || 24
  const pageStart = page * pageSize
  const pageEnd = pageStart + pageSize
  let result = req.query.category ? products.filter(food => food.category === req.query.category) : products
  if(req.query.minPrice) results = results.filter(f => f.price > req.query.minPrice)
  if(req.query.maxPrice) results = results.filter(f => f.price < req.query.maxPrice)
  res.send({
    data: result.slice(pageStart, pageEnd),
    meta: {
      pageStart,
      pageEnd,
      total: result.length
    }
  })
});

app.get('/food/:id/similarlyPriced', (req, res) => {
  const priceList = products.sort((a,b) => b.price - a.price) // sub-optimal
  const index = priceList.findIndex(p => p.id === req.params.id) // so i guess this could be
  res.send(priceList.slice(Math.max(0, index - 2), Math.min(priceList.length, index + 2)).filter(p => p !== req.params.id))
});

app.post('/food', (req, res) => {
  products.push(req.body)
  res.send(200);
});

app.use(express.static('../client/build')) // not for prod naturally


process.on('SIGINT', function () {
  process.exit();
});
