import { useState, useEffect } from 'react';

export default function App () {
  const [posting, setPosting] = useState(false)
  const [category, setCategory] = useState(null)
  const [products, setProducts] = useState([])
  const [page, setPage] = useState(0)
  const [total, setTotal] = useState(0)

  useEffect(() => {
    if (posting) {
      fetch('/food', {
        method: 'POST', 
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(Object.fromEntries(new FormData(document.forms['food-entry'])))
      }).then(res => {
        if (res.status === 200) {
          setPosting(false)
        } 
      }).catch(err => console.log(err) || setPosting(false))
    }
  }, [posting])

  useEffect(() => {
    const params = new URLSearchParams()
    if (category) params.set('category', category)
    if (page) params.set('page', page)
    fetch(`/food?${params}`).then(res => res.json()).then(({ data, meta }) => {
      setProducts(data)
      setTotal(meta.total)
    }).catch(console.error)
  }, [page, category])

  function onSubmit (event) {
    event.preventDefault()
    setPosting(true)
  }
  function incrementPage(event) {
    setPage(page + 1)
  }
  function decrementPage(event) {
    setPage(Math.min(0, page + 1))
  }
  function setCategoryFilter(event) {
    setCategory(event.target.value.trim())
  }
  return (
    <div className="app">
      <form id="food-entry" action="food" method="post"
        onSubmit={onSubmit}>
        <label>
          <span class="text">category:</span>
          <input list="categories" type="list" name="category"/><br/>
        </label>
        <label>
          <span class="text">name:</span>
          <input type="text" name="name"/><br/>
        </label>
        <br/>
        <label>
            <span class="text">price:</span>
            <input type="number" name="price"/><br/>
        </label><br/>
        <div class="align-right">
          <button type="submit">Submit</button>
        </div>
      </form>
      <input type="list" name="categoryFilter" list="categories" placeholder="Category filter" onChange={setCategoryFilter}></input>
      <datalist id="categories">
        <option value="fish" />
        <option value="meat" />
        <option value="greens" />
      </datalist>
      <span>contains: {total}</span>
      <ul>
        {products.map(({ name, category, price, id }) => (
          <li key={id}>
            <span>{name}</span>
            <span>{category}</span>
            <span>{price}</span>
          </li>
        ))}
        <div>
          {page}
          <button onClick={incrementPage}>get next</button>
          <button onClick={decrementPage}>prev next</button>
        </div>
      </ul>
    </div>
  )
}
